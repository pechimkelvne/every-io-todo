# every-io-todo

## Disclaimer

Sadly, I had a really busy week! I didn't want to delay it anymore. To gain some time I did not implement tools such as Docker or Jest for testing.

_And when straight ahead with inmemory sqlite just to avoid some hassle setuping migrations_ 😅

## How to run

```zsh
~ yarn && yarn local:watch
```

or with npm:

```zsh
~ npm i && npm run local:watch
```

## GraphQL Schema

### Access Service

```graphql
# =============== #
#      Types      #
# =============== #

# User describes the User entity.
type User {
  id: ID!
  username: String!
}

# AuthResponse describes the payload for authentication responses
type AuthResponse {
  token: String!
  user: User!
}

# =============== #
#     Queries     #
# =============== #

# Access_Auth authenticates the user
# Note: On this project, this common action doesn't generate backend
# side-effects so set up as a Query
Access_Auth(username: String!, password: String!): AuthResponse!

# Access_Me retrieves the current authenticated user
Access_Me: User!

# =============== #
#    Mutations    #
# =============== #

# Access_Register creates a new user
# Note: Responds the same way as the Access_Auth query
Access_Register(username: String!, password: String!): AuthResponse!
```

### Tasks Service

```graphql
# =============== #
#      Types      #
# =============== #

# TaskState indicates a task current state
enum TaskState {
  TODO
  INPROGRESS
  DONE
  ARCHIVED
}

# Task describes the Task entity.
type Task {
  id: ID!
  userId: ID!
  title: String!
  description: String
  state: TaskState!
}

# TaskStateUpdateResponse the response payload for Tasks_SetState
type TaskStateUpdateResponse {
  taskId: ID!
  state: TaskState!
}

# =============== #
#     Queries     #
# =============== #

# Tasks_List retrieves all tasks from the authenticated user
# Note: Optionally filters by state
Tasks_List(state: TaskState): [Task]!

# Tasks_Get retrieves the information about one single task
Tasks_Get(taskId: Int!): Task!

# =============== #
#    Mutations    #
# =============== #

# Tasks_Create creates a new task
Tasks_Create(title: String!, description: String!): Task!

# Tasks_Update updates data fields from a task
Tasks_Update(taskId: Int!, title: String, description: String): Task!

# Tasks_SetState updates a task state
Tasks_SetState(taskId: Int!, state: TaskState!): TaskStateUpdateResponse!
```