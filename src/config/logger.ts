import pino from 'pino';

let plogger: pino.Logger;

function initLogger({ level, name }: { level: string, name: string }): pino.Logger {
  plogger = pino({
    level,
    name,
  });

  return plogger;
}

function logger(): pino.Logger {
  if (!plogger) {
    return initLogger({
      level: 'info',
      name: 'every-io-todo',
    });
  }

  return plogger;
}

export { initLogger };

export default logger;
