import { Sequelize } from 'sequelize';

import getLogger from './logger';

const logger = getLogger();

function log(msg: string, opt: unknown): void {
  switch (typeof opt) {
    case 'number':
      logger.info({ timing: opt }, msg);
      break;
    case 'string':
      logger.info(msg, opt);
      break;
    case 'object':
      logger.info({}, msg);
      break;
    default:
      logger.warn(`Sequelize sent me an unexpected type: ${typeof opt}.`);
      logger.info({ meta: opt }, msg);
  }
}

function buildSequelizeInstance(): Sequelize {
  return new Sequelize(
    'sqlite::memory:',
    { logging: log },
  );
}

const sequelizeInstance = buildSequelizeInstance();

export { sequelizeInstance };
