interface ServerEnvironment {
  nodeEnv: string;
  port: string;
  jwtSecret: string;
}

const {
  NODE_ENV = 'development',
  PORT = '4000',
  JWT_SECRET = 'this_is_a_super_secret',
} = process.env;

const env: ServerEnvironment = {
  nodeEnv: NODE_ENV,
  port: PORT,
  jwtSecret: JWT_SECRET,
};

export type {
  ServerEnvironment,
};

export default env;
