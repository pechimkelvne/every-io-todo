import express, { Application, Request } from 'express';
import { json } from 'body-parser';
import expressPinoLogger from 'express-pino-logger';
import jwt, { JwtPayload } from 'jsonwebtoken';

import getLogger from '../config/logger';
import env from '../config/env';
import User from '../services/Access/models/User';

const logger = getLogger();

interface ApiContext {
  user?: User;
}

type AuthenticatedRequest = Request & ApiContext;

function loadAuthenticationMiddleware(app: Application) {
  app.use(async (request: AuthenticatedRequest, response, next) => {
    const { authorization } = request.headers;
    if (authorization && authorization.slice(0, 7) === 'Bearer ') {
      const token = authorization.split(' ')[1];
      try {
        const payload = jwt.verify(token, env.jwtSecret) as JwtPayload;
        request.user = await User.findByPk(payload.sub);
      } catch (e) {
        return response.sendStatus(401);
      }
    }
    return next();
  });
}

function createExpressApp(): Application {
  const app = express()
    .use(json())
    .use(expressPinoLogger({
      logger,
      autoLogging: env.nodeEnv !== 'testing',
    }));

  // Health Check handler
  app.get('/status', (_, response) => response.status(200).send({ data: 'Alive! :)' }));

  loadAuthenticationMiddleware(app);

  return app;
}

export { createExpressApp };

export type {
  AuthenticatedRequest,
  ApiContext,
};
