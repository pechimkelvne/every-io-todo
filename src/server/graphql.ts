import type { IResolvers } from '@graphql-tools/utils';

import accessServiceFactory from '../services/Access';
import accessServiceDefs from '../services/Access/graphql';

import tasksServiceFactory from '../services/Tasks';
import tasksServiceDefs from '../services/Tasks/graphql';

const typeDefs = `
${accessServiceDefs.typedefs}
${tasksServiceDefs.typedefs}

type Query {
  ${accessServiceDefs.queries}
  ${tasksServiceDefs.queries}
}

type Mutation {
  ${accessServiceDefs.mutations}
  ${tasksServiceDefs.mutations}
}
`;

const accessService = accessServiceFactory();
const tasksService = tasksServiceFactory();

const services = [accessService, tasksService];

function getResolvers(): IResolvers {
  const resolvers = services.reduce((result, service) => {
    const newResult: unknown = { ...result };
    Object.keys(service).forEach((key) => {
      if (typeof newResult[key] !== 'undefined') {
        newResult[key] = {
          ...newResult[key],
          ...service[key],
        };
      }
    });
    return newResult;
  }, {
    Query: {},
    Mutation: {},
  } as IResolvers);
  return resolvers as IResolvers;
}

export {
  typeDefs,
  getResolvers,
};
