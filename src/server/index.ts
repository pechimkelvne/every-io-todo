import type { Application, Response } from 'express';
import { ApolloServer } from '@apollo/server';
import { expressMiddleware } from '@apollo/server/express4';
import cors from 'cors';

import type { AuthenticatedRequest, ApiContext } from './express';
import { createExpressApp } from './express';
import { getResolvers, typeDefs } from './graphql';

export default async (): Promise<[Application, ApolloServer]> => {
  const expressApp = createExpressApp();
  const apolloServer = new ApolloServer<ApiContext>({
    typeDefs,
    resolvers: getResolvers(),
  });
  await apolloServer.start();
  expressApp.use('/graphql', cors(), expressMiddleware(apolloServer, {
    context: async ({ req }: { req: AuthenticatedRequest, res: Response }) => ({ user: req.user }),
  }));
  return [expressApp, apolloServer];
};
