import type { IResolvers } from '@graphql-tools/utils';

import type { ApiContext } from '../../server/express';
import User from '../Access/models/User';
import Task, { TaskState } from './models/Task';
import {
  SrlzdTaskListRequest,
  SrlzdTaskGetRequest,
  SrlzdTaskCreateRequest,
  SrlzdTaskUpdateRequest,
  SrlzdTaskSetStateRequest,
  SrlzdTaskStateUpdateResponse,
} from './types';

const notAuthenticatedError = new Error('Not authenticated!');

function checkUser(user?: User) {
  if (typeof user === 'undefined') {
    throw notAuthenticatedError;
  }
}

export default (): IResolvers => {
  // Init models
  Task.sync({ alter: true });

  return {
    TaskState: {
      TODO: TaskState.TODO,
      INPROGRESS: TaskState.INPROGRESS,
      DONE: TaskState.DONE,
      ARCHIVED: TaskState.ARCHIVED,
    },
    Query: {
      Tasks_List: async (
        _,
        { state }: SrlzdTaskListRequest,
        { user }: ApiContext,
      ): Promise<Task[]> => {
        checkUser(user);
        const where: Partial<Task> = { userId: user.id };
        if (typeof state !== 'undefined') {
          where.state = state;
        }
        return Task.findAll({ where });
      },
      Tasks_Get: async (
        _,
        { taskId }: SrlzdTaskGetRequest,
        { user }: ApiContext,
      ): Promise<Task> => {
        checkUser(user);
        return Task.findOne({ where: { id: taskId, userId: user.id } });
      },
    },
    Mutation: {
      Tasks_Create: async (
        _,
        payload: SrlzdTaskCreateRequest,
        { user }: ApiContext,
      ): Promise<Task> => {
        checkUser(user);
        return Task.create({ ...payload, userId: user.id });
      },
      Tasks_Update: async (
        _,
        payload: SrlzdTaskUpdateRequest,
        { user }: ApiContext,
      ): Promise<Task> => {
        checkUser(user);
        const set: Partial<Task> = Object.keys(payload).reduce((result, key) => {
          if (typeof payload[key] !== 'undefined' && payload[key] !== null) {
            return {
              ...result,
              [key]: payload[key],
            };
          }
          return result;
        }, {});
        await Task.update(
          set,
          { where: { id: payload.taskId, userId: user.id }, returning: ['*'] },
        );
        return Task.findByPk(payload.taskId);
      },
      Tasks_SetState: async (
        _,
        { state, taskId }: SrlzdTaskSetStateRequest,
        { user }: ApiContext,
      ): Promise<SrlzdTaskStateUpdateResponse> => {
        checkUser(user);
        await Task.update(
          { state },
          { where: { id: taskId, userId: user.id }, returning: true },
        );
        return {
          taskId,
          state,
        };
      },
    },
  };
};
