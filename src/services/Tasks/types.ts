import type { TaskState } from './models/Task';

interface SrlzdTaskUpdateRequest {
  taskId: number;
  title?: string;
  description?: string;
}

type SrlzdTaskCreateRequest = Required<Omit<SrlzdTaskUpdateRequest, 'taskId'>>

interface SrlzdTaskSetStateRequest {
  taskId: number;
  state: TaskState;
}

interface SrlzdTaskListRequest {
  state?: TaskState;
}

interface SrlzdTaskGetRequest {
  taskId: number;
}

interface SrlzdTask {
  id: number;
  title: string;
  description: string;
  state: TaskState;
}

type SrlzdTaskStateUpdateResponse = SrlzdTaskSetStateRequest;

export type {
  SrlzdTaskCreateRequest,
  SrlzdTaskGetRequest,
  SrlzdTaskListRequest,
  SrlzdTaskSetStateRequest,
  SrlzdTaskUpdateRequest,
  SrlzdTask,
  SrlzdTaskStateUpdateResponse,
};
