import { DataTypes, Model } from 'sequelize';

import { sequelizeInstance } from '../../../config/database';
import User from '../../Access/models/User';

enum TaskState {
  TODO = 'TODO',
  INPROGRESS = 'INPROGRESS',
  DONE = 'DONE',
  ARCHIVED = 'ARCHIVED',
}

class Task extends Model {
  declare id: number;

  declare title: string;

  declare description?: string;

  declare state: TaskState;

  declare userId: number;
}

Task.init({
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  title: {
    type: DataTypes.STRING(128),
    allowNull: false,
  },
  description: {
    type: DataTypes.TEXT,
    allowNull: true,
  },
  state: {
    type: DataTypes.ENUM('TODO', 'INPROGRESS', 'DONE', 'ARCHIVED'),
    allowNull: false,
    defaultValue: TaskState.TODO,
  },
  userId: {
    type: DataTypes.INTEGER,
    allowNull: false,
    references: {
      model: User,
      key: 'id',
    },
    field: 'user_id',
  },
}, {
  sequelize: sequelizeInstance,
  modelName: 'Task',
});

export default Task;

export {
  TaskState,
};
