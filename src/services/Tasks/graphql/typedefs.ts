export default `
# TaskState indicates a task current state
enum TaskState {
  TODO
  INPROGRESS
  DONE
  ARCHIVED
}

# Task describes the Task entity.
type Task {
  id: ID!
  userId: ID!
  title: String!
  description: String
  state: TaskState!
}

# TaskStateUpdateResponse the response payload for Tasks_SetState
type TaskStateUpdateResponse {
  taskId: ID!
  state: TaskState!
}
`;
