import mutations from './mutations';
import queries from './queries';
import typedefs from './typedefs';

export default {
  mutations,
  queries,
  typedefs,
};
