export default `
# Tasks_List retrieves all tasks from the authenticated user
# Note: Optionally filters by state
Tasks_List(state: TaskState): [Task]!

# Tasks_Get retrieves the information about one single task
Tasks_Get(taskId: Int!): Task!
`;
