export default `
# Tasks_Create creates a new task
Tasks_Create(title: String!, description: String!): Task!

# Tasks_Update updates data fields from a task
Tasks_Update(taskId: Int!, title: String, description: String): Task!

# Tasks_SetState updates a task state
Tasks_SetState(taskId: Int!, state: TaskState!): TaskStateUpdateResponse!
`;
