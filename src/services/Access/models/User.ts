import bcrypt from 'bcrypt';
import { addDays } from 'date-fns';
import { DataTypes, Model } from 'sequelize';
import jwt, { JwtPayload } from 'jsonwebtoken';

import { sequelizeInstance } from '../../../config/database';
import env from '../../../config/env';

class User extends Model {
  declare id: number;

  declare username: string;

  declare password: string;

  public isPasswordValid(password: string): Promise<boolean> {
    return bcrypt.compare(password, this.password);
  }

  public authenticationToken(secret: string = env.jwtSecret): string {
    const now = new Date();
    const payload: JwtPayload = {
      exp: addDays(now, 7).getTime(),
      iat: now.getTime(),
      iss: 'todo-node-backend',
      sub: this.id.toString(),
    };
    return jwt.sign(payload, secret);
  }
}

User.init({
  id: {
    type: DataTypes.INTEGER,
    autoIncrement: true,
    primaryKey: true,
  },
  username: {
    type: DataTypes.STRING(31),
    allowNull: false,
    unique: true,
  },
  password: {
    type: DataTypes.STRING,
    allowNull: false,
  },
}, {
  sequelize: sequelizeInstance,
  modelName: 'User',
  hooks: {
    async beforeSave(instance) {
      if (instance.changed('password')) {
        const salt = await bcrypt.genSalt();
        instance.set('password', await bcrypt.hash(instance.password, salt));
      }
    },
  },
});

export default User;
