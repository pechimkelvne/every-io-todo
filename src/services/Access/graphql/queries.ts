export default `
# Access_Auth authenticates the user
# Note: On this project, this common action doesn't generate backend
# side-effects so set up as a Query
Access_Auth(username: String!, password: String!): AuthResponse!

# Access_Me retrieves the current authenticated user
Access_Me: User!
`;
