export default `
# User describes the User entity.
type User {
  id: ID!
  username: String!
}

# AuthResponse describes the payload for authentication responses
type AuthResponse {
  token: String!
  user: User!
}
`;
