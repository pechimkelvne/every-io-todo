export default `
# Access_Register creates a new user
# Note: Responds the same way as the Access_Auth query
Access_Register(username: String!, password: String!): AuthResponse!
`;
