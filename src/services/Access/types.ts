interface SrlzdUserCredentialsRequest {
  username: string;
  password: string;
}

interface SrlzdUser {
  id: number;
  username: string;
}

interface SrlzdAuthResponse {
  token: string;
  user: SrlzdUser;
}

export type {
  SrlzdUserCredentialsRequest,
  SrlzdUser,
  SrlzdAuthResponse,
};
