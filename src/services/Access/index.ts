import type { IResolvers } from '@graphql-tools/utils';

import type { ApiContext } from '../../server/express';
import User from './models/User';
import { SrlzdAuthResponse, SrlzdUser, SrlzdUserCredentialsRequest } from './types';

const invalidCredentialsError = new Error('Invalid credentials.');

export default (): IResolvers => {
  // Init models
  User.sync({ alter: true });

  return {
    Query: {
      Access_Auth: async (
        _,
        { username, password }: SrlzdUserCredentialsRequest,
      ): Promise<SrlzdAuthResponse> => {
        const user = await User.findOne({ where: { username } });
        if (!user || !(await user.isPasswordValid(password))) {
          throw invalidCredentialsError; // instead of user not found because of opsec
        }
        return {
          user,
          token: user.authenticationToken(),
        };
      },
      Access_Me: async (_, __, { user }: ApiContext): Promise<SrlzdUser> => user,
    },
    Mutation: {
      Access_Register: async (_, payload): Promise<SrlzdAuthResponse> => {
        const user = await User.create(payload);
        return {
          user,
          token: user.authenticationToken(),
        };
      },
    },
  };
};
