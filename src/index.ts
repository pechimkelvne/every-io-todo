/* eslint-disable import/first */

import * as dotenv from 'dotenv';

dotenv.config();

import env from './config/env';
import logger from './config/logger';
import server from './server';

(async () => {
  const [app] = await server();
  app.listen(env.port, () => {
    logger().info(`Server running at port ${env.port}!`);
  });
})();
